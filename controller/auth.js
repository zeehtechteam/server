const mongoose = require('mongoose');

const usuarios = require('../model/user/usuarios');
const authLib = require('../libs/auth');

exports.login = (req, res, next) => {
  const usuario = req.body.usuario;
  const senha = req.body.senha;
  // authLib.decodeToken(req.cookies['xtoken'])
  //   .then(token => console.log(token))
  //   .catch(err => console.log(err));
  if ('x-token' in req.headers && req.headers['x-token'] !== 'null') {
    console.log('Recebi um x-token: ' + req.headers['x-token']);
  }
  usuarios.findOne({login: usuario, senha}).then(r => {
    if (!r) {
      res.sendStatus(401); // unauthorized
      return;
    }
    const token = authLib.generateToken(r._id);
    res.set('authkey', token);
    res.sendStatus(200);
  }).catch(() => {
    res.sendStatus(404);
  });
};

exports.register = (req, res, next) => {
  const usuario = req.body.usuario;
  const senha = req.body.senha;
  const nome = req.body.nome;
  const telefone = req.body.telefone;
  usuarios.findOne({login: usuario}).then(r => {
    if (r) {
      res.sendStatus(409); // conflict
      return;
    }
  }).catch(() => {});
  usuarios.create({login: usuario, senha, nome, telefone, transactions: []}).then(() => {
    res.sendStatus(201); // created
  })
  .catch((error) => {
    console.log(error);
    res.sendStatus(500); // internal server error

  })
};
