import * as path from "path";
import { fileLoader, mergeTypes } from "merge-graphql-schemas";
console.log("dir name: ", __dirname);
const typesArray = fileLoader(path.join(__dirname, "./typeDefs"), {
  recursive: true
});

export default mergeTypes(typesArray, { all: true });
