export default `
  type ItemSetor {
    Nome: String
    Funcoes: [ItemFuncaoCargo]
  }
  type ItemFuncaoCargo {
    Nome: String
    RiscosOcupacionais: [ItemRiscoOcupacional]
  }
  type ItemRiscoOcupacional {
    Nome: String
    AgentesDeRisco: [ItemAgenteDeRisco]
  }
  type ItemAgenteDeRisco {
    Nome: String
    FormasDeAvaliacao: [String]
    ValoresEncontrados: [String]
    FontesGeradoras: [String]
    ConsequenciasPossiveis: [String]
    TiposDeExposicao: [String]
    MeiosDeControleExistentes: [String]
    MeiosDeControlePropostos: [String]
  }

  type PPRA {
    Empresa: Empresa
    Setores: [ItemSetor]
    Funcionarios: Int
  }
`;
