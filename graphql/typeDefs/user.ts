export default `
scalar Date
scalar JSON

type Transaction {
  id: Int!
  date: Date
  approvedDate: Date
  status: Int
}

type AutoIncrements {
  cargos: Int
  cbos: Int
  cnaes: Int
  empresas: Int
  setores: Int
}

type Items {
  ppras: Int
  pcmsos: Int
}

type User {
  id: String
  login: String
  password: String
  name: String
  email: String
  phone: String
  items: Items
  ppras: [PPRA]
  transactions: [Transaction]
  userData: UserData
  autoIncrements: AutoIncrements
}

type Query {
  users: [User],
  user: User,
  userInfo: User,
  getData(dataType: String!): JSON
}

input UserInput {
  login: String!
  password: String!
  name: String!
  email: String!
  phone: String!
}

input DataInput {
  dataType: String!
  payload: JSON
}

type Mutation {
  register(input: UserInput!): String
  login(login: String!, password: String!): String
  dataAdd(input: DataInput!): JSON
  dataRemove(input: DataInput!): JSON
  dataUpdate(input: DataInput!): JSON
}
`;
