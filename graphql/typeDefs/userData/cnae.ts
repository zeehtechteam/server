export default `

type CNAE {
  id: Int! @unique
  descricao: String
  grauDeRisco: Int
}

`;
