export default `

type Empresa {
  id: Int!
  razaoSocial: String
  nomeFantasia: String
  cnpj: String
  email: String
  site: String
  imu: String
  cnae: String
  cep: String
  grauDeRisco: String
  endereco: String
  numero: String
  complemento: String
  caixaPostal: String
  bairro: String
  cidade: String
  estado: String
  categoria: String
  cipa: Boolean
}

`;
