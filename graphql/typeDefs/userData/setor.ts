export default `

type Setor {
  id: Int! @unique
  descricao: String
  grauDeRisco: Int
}

`;
