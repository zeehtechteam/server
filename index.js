const express = require("express");
const app = express();
const router = express.Router();
const cors = require("cors");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const config = require("./config");

mongoose.set("useCreateIndex", true);
mongoose.connect(
  config.connectionString,
  { useNewUrlParser: true }
);

// Carrega as rotas
const gerenciamento = require("./route/gerenciamento");
const auth = require("./route/auth");

app.use((req, res, next) => {
  res.header("Access-Control-Expose-Headers", "authkey");
  next();
});
// ativar cors
app.use(cors());

// Bodyparser
app.use(bodyParser.json()); // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ extended: false })); // url-encoded

app.use("/api/auth/", auth);

app.use("/api/gerenciamento/:table", gerenciamento);

app.get("*", function(req, res) {
  res.sendStatus(404); // not found
});

app.listen(8090);
