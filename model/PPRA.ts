type Empresa = {
  RazaoSocial: string;
};

type ItemSetor = {
  Nome: string;
  Funcoes: ItemFuncaoCargo[];
};
type ItemFuncaoCargo = {
  Nome: string;
  RiscosOcupacionais: ItemRiscoOcupacional[];
};
type ItemRiscoOcupacional = {
  Nome: string;
  AgentesDeRisco: ItemAgenteDeRisco[];
};
type ItemAgenteDeRisco = {
  Nome: string;
  FormasDeAvaliacao: string[];
  ValoresEncontrados: string[];
  FontesGeradoras: string[];
  ConsequenciasPossiveis: string[];
  TiposDeExposicao: string[];
  MeiosDeControleExistentes: string[];
  MeiosDeControlePropostos: string[];
};

type PPRA = {
  Empresa: Empresa;
  Setores: ItemSetor[];
  Funcionarios: number;
};
