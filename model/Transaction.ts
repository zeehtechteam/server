interface Transaction {
  id: number;
  date: Date;
  approvedDate: Date;
  status: number;
}

export default Transaction;
