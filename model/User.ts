import Items from "./Items";
import Transaction from "./Transaction";
import UserData from "./UserData";

interface AutoIncrements {
  cargos: number;
  cbos: number;
  cnaes: number;
  empresas: number;
  setores: number;
}

interface User {
  id: string;
  login: string;
  password: string;
  name: string;
  email: string;
  phone: string;
  ppras: PPRA[];
  items: Items;
  transactions: Transaction[];
  userData: UserData;
  autoIncrements: AutoIncrements;
}

export default User;
