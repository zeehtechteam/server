import Cargo from "./userData/Cargo";
import CBO from "./userData/CBO";
import CNAE from "./userData/CNAE";
import Empresa from "./userData/Empresa";
import Setor from "./userData/Setor";

interface UserData {
  cargos: Cargo[];
  cbos: CBO[];
  cnaes: CNAE[];
  empresas: Empresa[];
  setores: Setor[];
}

export default UserData;
