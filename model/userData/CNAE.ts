interface CNAE {
  id: number;
  descricao: string;
  grauDeRisco: number;
}

export default CNAE;
