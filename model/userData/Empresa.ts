interface Empresa {
  id: number;
  razaoSocial: string;
  nomeFantasia: string;
  cnpj: string;
  email: string;
  site: string;
  imu: string;
  cnae: string;
  cep: string;
  grauDeRisco: string;
  endereco: string;
  numero: string;
  complemento: string;
  caixaPostal: string;
  bairro: string;
  cidade: string;
  estado: string;
  categoria: string;
  cipa: boolean;
}

export default Empresa;
