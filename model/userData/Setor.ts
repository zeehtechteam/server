interface Setor {
  id: number;
  descricao: string;
  grauDeRisco: number;
}

export default Setor;
