const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    descricao: {
      type: String,
      required: true,
      unique: true,
      index: true
    }
});

module.exports = mongoose.model('cargos', schema);
