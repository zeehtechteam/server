const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    descricao: {
      type: String,
      required: true,
      unique: true,
      index: true
    },
    nome: {
      type: String,
      required: true
    }
});

module.exports = mongoose.model('cbos', schema);
