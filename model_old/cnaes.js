const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    descricao: {
      type: String,
      required: true,
      unique: true,
      index: true
    },
    grauDeRisco: {
      type: Number,
      required: true
    }
});

module.exports = mongoose.model('cnaes', schema);
