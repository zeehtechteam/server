const mongoose = require('mongoose');

module.exports = new mongoose.Schema({
  data: Date,
  dataPagamentoAprovado: Date,
  statusPagamento: Number
});