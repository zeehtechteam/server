const express = require('express');
const router = express.Router({mergeParams: true});
const controller = require('../controller/gerenciamento');

router.get('/', (req, res, next) => controller.get(req, res, next));
router.post('/', (req, res, next) => controller.post(req, res, next));
router.patch('/', (req, res, next) => controller.patch(req, res, next));
router.delete('/:id', (req, res, next) => controller.delete(req, res, next));

module.exports = router;
